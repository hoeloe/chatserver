const WebSocket = require('ws');
const Axios = require('axios');
const http = require('http');
const https = require('https');
const fs = require('fs');
const Auth = require('./Auth');
const FormData = require('form-data');
const RateLimiter = require('./RateLimiter');
const Commands = require('./commands');
const LoggerService = require('./LoggerService');
const Messages = require('./serverMessages');

module.exports = class ChatServer {


    constructor(enableCommands = false, enableLogs = false, enableSsl = false, enableAuth = true) {
        this.logger = new LoggerService(enableLogs, 'server: ');
        this.serverKey = process.env.CHAT_SERVER_KEY;
        this.port = process.env.CHAT_SOCKET_PORT;
        this.appUrl = process.env.APP_URL;
        if (enableCommands) {
            this.commands = new Commands();
            this.commands.listen().on('ls chats', (e) => {
                this.logger.log(e);
                this.logger.log(this.activeChats, this.chatIds);
            }).on('ls servicers', () => {
                this.logger.log(this.activeServicers, this.servicerIds);
            });
        }

        if (!enableSsl) {
            this.server = http.createServer();
        } else {
            this.server = https.createServer({
                cert: fs.readFileSync(process.env.CHAT_SSL_CERT),
                key: fs.readFileSync(process.env.CHAT_SSL_KEY)
            });
        }

        this.wss = new WebSocket.Server({
            server: this.server,
        });
        this.auth = new Auth(this.serverKey);

        this.activeChats = [];
        this.chatIds = [];

        this.activeServicers = [];
        this.servicerIds = [];

        this.acceptedFunctions = {
            'servicer': [
                'handleServicerJoinsChat',
                'handleNewServicer',
                'handleAnsweredQuestion',
                'handleChatMessage',
                'handleClientJoinsChat',
                'handleClientOpensChat',
                'handleEndConversation',
                'handleIsTyping',
                'handleReadByServicer',
                'handleReadMessage',
                'handleSaveConversation',
                'handleSystemMessage',
            ],
            'client': [
                'handleAnsweredQuestion',
                'handleChatMessage',
                'handleClientJoinsChat',
                'handleClientOpensChat',
                'handleEndConversation',
                'handleIsTyping',
                'handleReadMessage',
                'handleSaveConversation',
                'handleSystemMessage',
            ]
        };

        this.burstSize = 60;
        this.baseRate = 100;

    }

    init(port) {
        this.wss.on('connection', (ws, req) => {
            if (this.enableAuth) {
            if (! this.auth.checkRequest(req, ws) && this.enableAuth) {
                return;
            }
            }
            this.logger.log('ws:: ', req.url.substring(8));
            const rateLimiter = new RateLimiter(this.burstSize, this.baseRate);
            ws.on('message', (message) => {
                this.logger.log(message);
                rateLimiter.input(message);
            });
            ws.on('close', () => {
                this.logger.log('connection closed\n >> ');
            });
            ws.on('error', (e) => {
                this.logger.log(`something went wrong: ${e}`);
            });

            this.auth.checkUser(req).then((userType) => {
                if (!this.enableAuth) {
                    this.logger.log('user authenticated without check')
                   this.acceptConnection(ws, 'servicer', rateLimiter);
                    return
                }
                this.logger.log('dit is e: ', userType);
                if (!userType) {
                    ws.terminate();
                } else {
                    this.acceptConnection(ws, userType, rateLimiter);
                }
            });
        });
        this.server.listen(port);
        this.logger.log('start listening onport : ', port)
    }

    acceptConnection(ws, userType, rateLimiter) {
        const functions = this.acceptedFunctions[userType];
        this.logger.log({userType});
        this.logger.log(this.acceptedFunctions);

        rateLimiter.output.on('packet', (data) => {
            let message = JSON.parse(data);
            if (!functions.includes(message.fn)) {
                this.logger.log(`${message.fn} not allowed`);
                return;
            }
            this.logger.log(`${message.fn} allowed`);
            try {
                this.logger.log(message.fn);
                eval('this.' + message.fn).bind(this)(ws, message);
            } catch (e) {
                this.logger.log('something went wrong: ', e);
            }
        });
        rateLimiter.output.on('drop', (data) => {
            this.logger.log('dropped', data.chatId);
            ws.close();
        });
        ws.send(JSON.stringify(this.createMessage('', 'connectionAccepted', true)));
    }


////////////////////////////////// handler functions /////////////////////////////////////////

    handleEndConversation(ws, message) {
        let content = {
            question: 'How would you rate this conversation?',
            answers: [1, 2, 3, 4, 5]
        };
        let msg = this.createChatMessage(message.chatId, content, 'server', 'question');
        ws.send(JSON.stringify(msg));
    }

    async handleSaveConversation(ws, message) {

        this.logger.log('saved');
        await this.saveConversation(this.activeChats[message.chatId]);

        let msg = {
            fn: 'savedConversation',
            chatId: message.chatId,
            timestamp: new Date().getTime(),
            sender: 'server',
            isClient: false,
            content: 'Saved chat',
            type: 'system'
        };
        ws.send(JSON.stringify(msg));
        this.deleteChat(message.chatId);

    }

    handleAnsweredQuestion(ws, message) {
        if (message.type === 'rating') {
            this.activeChats[message.chatId].rating = message.content;
            let msg = this.createMessage(message.chatId, 'handleChatMessage', Messages.rating, Messages.sender);

            ws.send(JSON.stringify(msg));
        } else if (message.type === 'note') {
            this.activeChats[message.chatId].clientNote = message.content;
        }
    }

    handleChatMessage(ws, message) {
        this.logger.log(`Received message => ${message.sender.fName}: ${message.content}\n >> `);
        // message.fn = 'chatMessage'
        try {
            this.activeChats[message.chatId].messages.push(message);
            this.activeChats[message.chatId].ws.send(JSON.stringify(message));
            this.servicerIds.forEach(servicer => {
                this.activeServicers[servicer].ws.send(JSON.stringify(message));
            });
        } catch (e) {
            this.logger.log('failed to handle message: ', e);
        }
    }

    handleServicerJoinsChat(ws, message) {
        this.activeChats[message.chatId].messages.forEach(msg => {
            ws.send(JSON.stringify(msg));
        });
        this.activeChats[message.chatId].servicer = message.sender;
        this.activeChats[message.chatId].servicerWss.push(ws);
    }

    handleReadByServicer(ws, message) {
        let msg;
        if (this.activeChats[message.chatId]) {
            this.activeChats[message.chatId].unreadCount = 0;
            this.activeChats[message.chatId].servicer = message.sender;
            msg = this.createMessage(message.chatId, 'updateUnread', '');
        } else {
            msg = this.createMessage(message.chatId, 'close', '');
        }
        this.sendToAllServicers(msg);
    }

    handleReadMessage(ws, message) {
        let msg = this.createMessage(
            message.chatId,
            'handleMessageWasRead',
            message.content,
            message.sender,
            'system',
            message.isClient
        );
        this.activeChats[message.chatId].messages.map(msg => {
            msg.read = 1;
        });
        if (message.isClient) {
            this.sendToAllServicers(msg);
        } else {
            this.activeChats[message.chatId].ws.send(JSON.stringify(msg));
        }
    }

    handleIsTyping(ws, message) {
        this.logger.log('...');
        let msg = this.createMessage(
            message.chatId,
            'handleIsTyping',
            '...',
            message.sender,
            'system',
            message.isClient
        );
        if (message.isClient) {
            this.sendToAllServicers(msg);
        } else {
            this.activeChats[message.chatId].ws.send(JSON.stringify(msg));
        }
    }

    handleClientOpensChat(ws, message) {
        this.registerClient(ws, message, true);
    }

    handleClientJoinsChat(ws, message) {
        this.registerClient(ws, message);
    }

    handleNewServicer(ws, message) {
        this.logger.log('new servicer');
        if (! this.servicerIds.includes(message.content)) {
            this.servicerIds.push(message.content);
            this.activeServicers[message.content] = {
                id: message.content,
                info: message.sender,
                ws: ws,
                signOffTimeout: ''
            };
        } else {
            clearTimeout(this.activeServicers[message.content].signOffTimeout);
        }
        ws.on('close', () => {
            this.activeServicers[message.content].signOffTimeout = setTimeout(() => {
                this.deleteServicer(message.content);
                this.logger.log('signed off servicer', message.content);
            }, 10000);
        });
        this.sendChatsToServicer(ws, this.activeChats);
    }

    ////////////////////////////////////// support functions /////////////////////////////////////

    registerClient(ws, message, sendMessages = false) {
        if (this.activeChats[message.chatId] !== undefined) {
            this.logger.log('existing chat\n >> ');
            this.activeChats[message.chatId].clientLocation = message.content;
            this.activeChats[message.chatId].ws = ws;
            if (sendMessages) {
                this.activeChats[message.chatId].messages.forEach(msg => {
                    ws.send(JSON.stringify(msg));
                });
            }
        } else {
            this.logger.log('new chat\n >> ');
            this.newClientConnection(ws, message);
        }
        ws.send(JSON.stringify(this.getOnlineServicer()));
        this.sendToAllServicers(this.createMessage(
            message.chatId,
            'updateClientLocation',
            message.content,
            message.sender,
            'object'
        ));

    }

    getOnlineServicer() {
        let c;
        if (this.servicerIds.length) {
            let { sender } = this.activeServicers[this.servicerIds[this.servicerIds.length - 1]];
            c = sender;
        } else {
            c = false;
        }
        return this.createMessage('', 'showOnlineServicer', c, '');
    }


    deleteChat(id) {
        let msg = {
            fn: 'endChat',
            chatId: id,
            timestamp: new Date().getTime(),
            sender: 'server',
            isClient: false,
            content: '**Gesprek beeindigd**'
        };
        this.activeChats[id].ws.send(JSON.stringify(msg));
        this.sendToAllServicers(msg);
        try {
            delete this.activeChats[id];
            this.chatIds.splice(this.chatIds.indexOf(id), 1);
        } catch (e) {
            this.logger.log(e);
        }
    }

    deleteServicer(id) {
        delete this.activeServicers[id];
        this.servicerIds.splice(this.servicerIds.findIndex((x) => {
            return x === id;
        }), 1);
    }

    async saveConversation(conversation) {
        const formData = new FormData();

        formData.append('messages', JSON.stringify(conversation.messages));
        formData.append('start_time', conversation.startTime);
        formData.append('end_time', conversation.endTime);
        formData.append('employee', conversation.servicer.userId || 0);
        formData.append('client', conversation.client.userId);
        formData.append('debtor_id', conversation.client.debtorId);
        formData.append('rating', conversation.rating);
        formData.append('key', this.serverKey);

        Axios.post(`${this.appUrl}/api/chat/store`, formData, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
            }
        }).then((response) => {
            this.logger.log('saved', response);


        }).catch(e => {
            this.logger.log(e.statusText);
        });

    }


    newClientConnection(ws, message) {
        this.activeChats[message.chatId] = {
            id: message.chatId,
            ws: ws,
            clientLocation: message.content,
            servicerWss: [],
            messages: [],
            startTime: new Date().getTime(),
            endTime: 0,
            client: message.sender,
            unreadCount: 1,
            readStatus: 0,
            servicer: false,
            rating: 0,
            note: ''
        };
        this.chatIds.push(message.chatId);
        const { id, client, servicer, messages, unreadCount, startTime, clientLocation } = this.activeChats[message.chatId];
        const msg = this.createMessage('', 'newChat',
            { id, client, servicer, messages, unreadCount, startTime, clientLocation });
        this.sendToAllServicers(msg);
        this.logger.log('opened conversation ' + message.chatId + '\n >> ');
    }

    handleSystemMessage(ws, message) {
        this.logger.log(`system: ${message.content}`);
    }

    sendToAllServicers(msg) {
        try {
            this.servicerIds.forEach(servicerId => {
                this.activeServicers[servicerId].ws.send(JSON.stringify(msg));
            });
        } catch (e) {
            this.logger.log('Could not send message :' + msg.fn + ' to servicer', e);
        }
    }

    broadcast(text) {
        let message = {
            chatId: 'broadcast',
            timestamp: new Date().getTime(),
            sender: 'server',
            isClient: false,
            content: 'server: ' + text.slice(4)
        };
        this.chatIds.forEach((chatId => {
            this.activeChats[chatId].ws.send(JSON.stringify(message));
        }));
        this.logger.log('Broadcasting: ' + text + '\n >> ');
    }

    createChatMessage(conversationId, content, sender = 'server', type = 'text', isClient = false) {
        return {
            chatId: conversationId,
            timestamp: new Date().getTime(),
            sender: sender,
            isClient: isClient,
            content: content,
            type: type
        };
    }

    createMessage(conversationId, fn, content, sender = 'server', type = 'text', isClient = false) {
        return {
            fn: fn,
            chatId: conversationId,
            timestamp: new Date().getTime(),
            sender: sender,
            isClient: isClient,
            content: content,
            type: type
        };
    }


    sendChatsToServicer(ws, chats) {
        let chatlist = [];
        for (let chat in chats) {
            let { id, client, servicer, messages, unreadCount, startTime, clientLocation } = this.activeChats[chat];
            chatlist.push({ id, client, servicer, messages, unreadCount, startTime, clientLocation });
        }
        const msg = this.createMessage('', 'allChats', chatlist);
        ws.send(JSON.stringify(msg));
    }


};
