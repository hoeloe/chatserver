const Axios = require('axios');
const LoggerService = require("./LoggerService.js");

module.exports = class Auth {

    constructor(key) {
        this.logger = new LoggerService(process.env.APP_DEBUG, 'auth: ');
        this.origin = process.env.APP_URL;
        this.key = key;
        this.axios = Axios;

    }

    checkRequest(req, ws) {
        if (!(req.headers.origin === this.origin)) {
            this.logger.log('declined connection = require' +
                    req.headers.origin +
                    ' to ' +
                    this.origin);
            ws.terminate();
            return false;
        } else {
            this.logger.log('accepted');
            return true;
        }
    }

    async checkUser(req) {
        let Cookie = req.url.substring(8);
        this.logger.log(Cookie);
        return this.axios.get(`${this.origin}/api/chat/authenticateUser`, {
            headers: {
                Cookie
            }
        }).then((res) => {
            this.logger.log(res.status);
            this.logger.log(res.data);
            return res.data;
        }).catch(e => {
            this.logger.log(e);
            this.logger.log(e.response.data);
            return false;
        });
    }

};
