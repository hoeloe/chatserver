const EventEmitter = require('events');
const LoggerService = require("./LoggerService");

module.exports = class RateLimiter {

    constructor(maxTokens, baseRate) {
        this.logger = new LoggerService(process.env.APP_DEBUG, 'ratelimiter: ');
        this.tokens = maxTokens;
        this.output = new EventEmitter();

        setInterval(() => {
            if (this.tokens < maxTokens) {
                this.tokens++;
            }
        }, baseRate);
    }

    input(e) {
        this.logger.log(this.tokens);
        if (this.tokens > 0) {
            --this.tokens;
            return this.output.emit('packet', e);
        } else {
            this.output.emit('drop', e);
        }
    }
};