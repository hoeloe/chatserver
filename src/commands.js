const EventEmitter = require('events');

module.exports = class Commands {

    constructor() {
        this.stdin = process.openStdin();
        this.engaged = false;
        this.emitter = new EventEmitter();

    }

    showHelp = () => {
        console.log('commands: \n\n' +
                'help                            -   show this\n' +
                'msg:CONVERSATION_ID: MESSAGE    -   send message to conversation\n' +
                'bct: MESSAGE                    -   broadcast to all conversations'
        );
    };

    sendMessageToChat(data, client = true) {
        try {
            console.log('message to: ' + data.slice(4, 12));
            if (client) {
                let sender = 'servicer';
                let target = activeChats[data.slice(4, 12)];
            } else {
                let sender = 'client';
                let target = activeServicers[data.slice(4, 12)];
            }
            let message = createChatMessage(data.slice(4, 12), data.slice(13), sender);
            target.ws.send(JSON.stringify(message));
        } catch (e) {
            console.log('there was an error in your command:\n', e);
        }
    }


    handleInput(input) {
        switch (true) {
            case (data === 'ls chats'):
                console.log(activeChats);
                break;
            case (data === 'ls servicers'):
                console.log(activeServicers);
                break;
            case (data.slice(0, 5) === 'bct: '):
                broadcast(input.slice(5));
                break;
            case (data === 'help'):
                showHelp();
                break;
            case (data.slice(0, 4) === 'msg:'):
                sendMessageToChat(input.slice(5));
                break;
            case (data.slice(0, 4) === 'srv:'):
                sendMessageToChat(input.slice(5), false);
                break;

        }
    }

    listen() {
        this.stdin.addListener('data', (d) => {
            let data = d.toString().trim();
            this.emitter.emit(data);
            // commands:
            if (this.engaged !== false) {
                if (data === 'exit') {
                    this.engaged = false;
                }
            } else if (data === 'ls chats') {


            } else if (data === 'ls servicers') {

            } else if (data.slice(0, 5) === 'bct: ') {

            } else if (data === 'help') {

            } else if (data.slice(0, 4) === 'msg:') {

            } else if (data.slice(0, 4) === 'srv:') {

            } else if (data.slice(0, 7) === 'engage:') {

            }
        });
        return this.emitter;
    }
};

