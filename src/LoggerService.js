module.exports = class LoggerService {

    constructor(debug, prefix) {
        this.debug = debug;
        this.prefix = prefix;
    }

    log(...args) {
        if (this.debug) {
            console.log(`${this.prefix}`, ...args);
        }
    }
};