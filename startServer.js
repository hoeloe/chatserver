require('dotenv').config();
const yargs = require('yargs')
const ChatServer = require('./src/ChatServer');
const argv = yargs
   .command('listen', 'specify port for server', {
        port: {
            description: 'port to listen on',
            alias: 'p',
            type: 'number'
        }
    })
    .option('ssl', {
        alias: 's',
        description: 'enables ssl',
        type: 'boolean'
    })
    .option('logs', {
        alias: 'l',
        description: 'enables logging to stdout',
        type: 'boolean'
    }).option('commands', {
        alias: 'c',
        description: 'enables use of commands in stdin',
        type: 'boolean'
    })
    .help().alias('help', 'h').argv
const options = {
    enableCommands: argv.commands,
    enableLogs: argv.logs,
    enableSsl: argv.ssl | process.env.USE_SSL
}

const server = new ChatServer(argv.commands, argv.logs, argv.ssl);
console.log(argv)
server.init(argv.port);
